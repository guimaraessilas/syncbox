package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server extends Thread {

    private static ArrayList<BufferedWriter> clients;
    private static ServerSocket server;
    private final Socket con;
    private InputStream in;
    private InputStreamReader inr;
    private BufferedReader bfr;
    private String contentFile = "";
    
    public static void main(String[] args) {
        try {
            server = new ServerSocket(5555);
            clients = new ArrayList<>();

            while (true) {
                System.out.println("Waiting client...");
                Socket con = server.accept();
                System.out.println("Client connected");
                Thread t = new Server(con);
                t.start();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public Server(Socket con) {
        this.con = con;
    }

    @Override
    public void run() {
        try {
            while(true){
                String msg;
                in = con.getInputStream();
                inr = new InputStreamReader(in);
                bfr = new BufferedReader(inr);
                OutputStream ou = this.con.getOutputStream();
                Writer ouw = new OutputStreamWriter(ou);
                BufferedWriter bfw = new BufferedWriter(ouw);
                clients.add(bfw);
                msg = bfr.readLine();
                sync(bfw, msg);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void sync(BufferedWriter bwSaida, String msg) throws IOException {
        System.out.println("Syncronizing...");
        fileManager(msg.split(":")[0], msg.split(":")[1], msg.split(":")[2]);
    }
    
    public void fileManager(String action, String filename, String content) {

        if(filename.contains("client1/")){
            filename = filename.replace("client1/", "client2/");
        }else if(filename.contains("client2/")){
            filename = filename.replace("client2/", "client1/");
        }
        
        if(!content.trim().isEmpty()){
                content = content.substring(1, content.length());
                content = content.replace("'nl'", "\n");
            
        }else{
            content = "";
        }
        
        try {
            File file = new File(filename);
            
            System.out.println("Action: " + action);
            System.out.println("File: " + filename);
            System.out.println("Content: " + content);

            if (action.equals("ENTRY_DELETE")) {
                file.delete();
            }
            
            if (action.equals("ENTRY_CREATE")) {
                file.createNewFile();
                
                if(!content.isEmpty()){

                    FileWriter arq = new FileWriter(filename);
                    PrintWriter gravarArq = new PrintWriter(arq);
                    gravarArq.printf(content);
                    arq.close();
                }
                
            }

            if(action.equals("ENTRY_MODIFY")){
                System.out.println("Modificando...");
            }
            
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
