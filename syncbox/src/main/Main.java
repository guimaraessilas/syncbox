package main;

import java.io.IOException;
import java.net.Socket;

public class Main {
    
    public static void main(String[] args) throws IOException {
        
        Client client1 = new Client(new Socket("localhost", 5555), "/home/silas/Documents/client1/");
        client1.start();
        Client client2 = new Client(new Socket("localhost", 5555), "/home/silas/Documents/client2/");
        client2.start();
        
    }
 
}
