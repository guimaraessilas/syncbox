package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.Map;

public class Client extends Thread{

    private OutputStream out;
    private String localPath;
    
    public Client(Socket socket, String localPath){
        try{
            this.out = socket.getOutputStream();
            this.localPath = localPath;
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void run(){
        fileListener();
    }
    
    public void fileListener() {
        try (WatchService service = FileSystems.getDefault().newWatchService()) {
            Map<WatchKey, Path> keyMap = new HashMap<>();
            Path path = Paths.get(localPath);
            keyMap.put(path.register(service,
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_DELETE,
                    StandardWatchEventKinds.ENTRY_MODIFY
                    ),
                    path);

            WatchKey watchKey;

            do {

                watchKey = service.take();
                
                for (WatchEvent<?> event : watchKey.pollEvents()) {
                    if(!event.context().toString().contains("goutputstream")){

                        WatchEvent.Kind<?> kind = event.kind();
                        String msgClient = kind.toString() + ":" + keyMap.get(watchKey) + "/" + event.context()+":";
                        
                        String content = " ";
                        if(kind.toString().equals("ENTRY_MODIFY") || kind.toString().equals("ENTRY_CREATE")){
                            FileReader arq = new FileReader(keyMap.get(watchKey) + "/" + event.context());
                            BufferedReader lerArq = new BufferedReader(arq);
                            

                            String linha = lerArq.readLine();
                            
                            while (linha  != null) {
                                content += linha+"'nl'";
                                linha = lerArq.readLine();
                            }
                            
                            arq.close();
                        }
                        
                        msgClient += content;
                        
                        System.out.println("Mensagem Cliente: "+msgClient);
                        BufferedWriter bufOut = new BufferedWriter(new OutputStreamWriter(out));
                        bufOut.write(msgClient);
                        bufOut.newLine();
                        bufOut.flush();
                    }
                }
                
            } while (watchKey.reset());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}